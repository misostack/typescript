# Install

```bash
npm install -g typescript
```

**Check version**

```bash
tsc -v
```

**Compile**
```bash
tsc hello/index.ts
```

**Hello world**

```javascript
function greeter(person) {
    return "Hello, " + person;
}

let user = "Jane User";

document.body.innerHTML = greeter(user);
```

### Ignore safecrlf

```
git config --local -l
git config --global -l

git config --local core.safecrlf = false
```