class Student {
    fullname : string;
    constructor(public firstname, public middleinitial, public lastname) {
        this.fullname = firstname + " " + middleinitial + " " + lastname;
    }
}

interface Person {
    firstname: string;
    lastname: string;
}

function greeting(person: Person){
    return "Welcome " + person.firstname + " " + person.lastname;
}

var student = new Student("Jane", ".M", "User");

document.body.innerHTML = greeting(student);