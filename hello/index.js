var Student = /** @class */ (function () {
    function Student(firstname, middleinitial, lastname) {
        this.firstname = firstname;
        this.middleinitial = middleinitial;
        this.lastname = lastname;
        this.fullname = firstname + " " + middleinitial + " " + lastname;
    }
    return Student;
}());
function greeting(person) {
    return "Welcome " + person.firstname + " " + person.lastname;
}
var student = new Student("Jane", ".M", "User");
document.body.innerHTML = greeting(student);
